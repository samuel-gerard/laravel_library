@extends('layouts.app')

@section('content')

{{ Breadcrumbs::render('home') }}

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    La librairie est un commerce de détail destiné à la vente de livres. 
                    Il existe différents types de points de vente du livre : librairies de livres neufs (généralistes ou spécialisées), 
                    librairies de livres anciens et d'occasion, maisons de la presse, librairies-papeteries, librairies ambulantes, 
                    grandes surfaces, librairies numériques.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
