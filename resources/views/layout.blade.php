<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
    </head>

    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item active"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                <li class="nav-item active"><a class="nav-link" href="{{ route('about') }}">About</a></li>
                <li class="nav-item active"><a class="nav-link" href="{{ route('books') }}">Livres</a></li>
                <li class="nav-item active"><a class="nav-link" href="{{ route('auteurs') }}">Auteurs</a></li>
                <li class="nav-item active"><a class="nav-link" href="{{ route('editor.index') }}">Éditeurs</a></li>
                <li class="nav-item active"><a class="nav-link" href="{{ route('categorie.index') }}">Catégories</a></li>
            </ul>
        </nav>

        @yield('content')
     <script src="{{ asset(mix('js/app.js')) }}"></script>
     <script src="{{ asset(mix('js/manifest.js')) }}"></script>
     <script src="{{ asset(mix('js/vendor.js')) }}"></script>
    </body>

</html>
