@extends('layouts/app')

@section('title')
    Editeurs
@endsection

@section('content')

{{ Breadcrumbs::render('editors') }}

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @can('create', \App\Editor::class)
                    <a class="btn btn-primary mb-4" href="{{ route('editor.create') }}">Ajouter un éditeur</a>
                @endcan
                <div class="card">
                    <div class="card-header">Liste des éditeurs :</div>
                        <div class="card-body">
                            <ul class="list-group-item">
                                @foreach ($editors as $editor) 
                                    @if ($editor)
                                        <li class="list-group-item">  
                                            <a href="{{ route('editor.show', ['editor'=>$editor]) }}">{{$editor->nom}}</a> 
                                        </li>   
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection