@extends('layouts/app')

@section('title')
Modification d'un éditeur
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h1>Modification d'un éditeur :</h1>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{ route('editor.update', ['editor'=>$editor]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input class="form-control" type="text" name="nom" placeholder="Nom de l'éditeur"
                        value="{{ old('editor', $editor->nom) }}">
                    <button class="btn btn-primary mt-2" type="submit">Valider</button>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection