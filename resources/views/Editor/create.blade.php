@extends('layouts/app')

@section('title')
Ajout d'un editeur
@endsection

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h1>Ajout d'un editeur :</h1>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{ route('editor.store') }}" method="POST">
                    @csrf
                    <input class="form-control" type="text" name="nom" placeholder="Nom de l'éditeur" value="{{ old('editor') }}">

                    <button class="btn btn-primary mt-2" type="submit">Valider</button>
                </form>

            </div>
        </div>
    </div>
</div>


@endsection