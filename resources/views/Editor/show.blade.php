@extends('layouts/app')

@section('title')
    Editeur 
@endsection

@section('content')

{{ Breadcrumbs::render('editor', $editor) }}

     <div class="container">

          <div class="row">
               <div class=".col-sm mr-2 mb-4">
                    @can('update', $editor)
                         <a class="btn btn-primary" href="{{ route('editor.edit', ['editor'=>$editor]) }}">Modifier l'éditeur</a>
                    @endcan
               </div>

               <div class=".col-sm">
                    @can('delete', $editor)
                         <form action="{{ route('editor.destroy', ['editor'=>$editor]) }}" method="POST">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-danger">Supprimer l'éditeur</button>
                         </form>
                    @endcan 
               </div>
          </div>

          
                 
          <div class="row justify-content-center">
              <div class="col-md-8">
                  <div class="card">
                      <div class="card-header">@if ($editor)
                                                  <h1>{{ $editor->nom }}</h1>
                                               @endif</div>
                          <div class="card-body">
                                   <h2>Livres : </h2>

                                   @if ($editor->books()->get()->isNotEmpty())
                                        <ul>
                                             @foreach ($editor->books as $book)
                                                  <li><a href="{{ route('book.show', ['book'=>$book]) }}">{{ $book->title }}</a></li>
                                             @endforeach
                                        </ul>
                                   @endif
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
@endsection