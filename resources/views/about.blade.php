@extends('layouts/app')

@section('title')
    About
@endsection

@section('content')

{{ Breadcrumbs::render('about') }}

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">A propos de nous :</div>
                    <div class="card-body">
                        La librairie moderne trouve son origine dans les stationes du Moyen Âge, sorte d'échoppes où des stationaires vendaient des manuscrits produits par des copistes. Le mot demeura dans la langue anglaise ((en) stationers) où stationery renvoie aux fournitures et papeterie, et library à bibliothèque1. En Europe, les premiers libraires-marchands demeurent près des universités : il existe un lien organique entre les clercs, les étudiants et le pouvoir royal, d'où d'importantes contraintes à la fois géographiques et législatives. Dès 1275, Philippe le Bel prononce une ordonnance chargeant l'Université de surveiller les libraires de Paris. En 1403, naît à Londres la Guild of Stationers. De fait, tous s'organisent en corporations sous la bannière de saint Jean Porte latine. Ils se voient interdits d'ouvrir plusieurs boutiques, de falsifier ou altérer des manuscrits, et de mettre en vente sans avoir au préalable subi la censure de la dite Université2. L'élément le plus important ici est qu'ils ne devaient pas exercer ce métier de façon itinérante, à la manière des colporteurs : ils « stationnaient » à un point fixe et tenaient boutique avec enseigne, précisant leurs spécialités ; leurs commis de chargeaient de livrer des clients ou d'aller chercher des manuscrits parfois sur des distances très éloignées. Une eau-forte de Pierre Adeline tirée des Vues de Paris (vers 1680) montre le Pont-Neuf couvert de petites boutiques en bois occupées par des libraires. L'apparition de l'imprimerie à caractères mobiles augmente la production de livres et la diffusion : les libraires sont tour à tour marchand, éditeur, imprimeur, relieur. Les clients de cette époque viennent chez le libraire soit pour acheter des livres déjà reliés, soit des feuilles, soit encore des cahiers cousus et protégés par une couverture en papier. La corporation regroupe donc plusieurs métiers associés dans la chaîne de fabrication du livre : typographe, imprimeur, correcteur, façonneur, relieur... et marchand1. 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection