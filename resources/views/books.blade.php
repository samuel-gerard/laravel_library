@extends('layouts/app')

@section('title')
    Books
@endsection

@section('content')

{{ Breadcrumbs::render('books') }}

<div class="container">
    @can('create', 'App\Book')
        <a class="btn btn-primary mb-4" href="{{ route('book.create') }}">Ajouter un livre</a>
    @endcan
    
    @foreach ($books as $book)
        <h2><a href="{{ route('book.show', ['book'=>$book]) }}">{{ $book->title }}</a></h2>
        @if ($book->auteur)
            <p>By <a href="{{ route('auteur.show', ['auteur'=>$book->auteur]) }}">{{ $book->auteur->prenom }} {{ $book->auteur->nom }}</a></p>
        @endif
        <p>{{ $book->summary }}</p>
    @endforeach
</div>
@endsection