@extends('layouts/app')

@section('title')
Modification d'un livre
@endsection

@section('content')

{{ Breadcrumbs::render('book.edit', $book) }}

<h1>Modification d'un livre :</h1>

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('book.update', ['book'=>$book]) }}" method="POST">
    @csrf
    @method('PUT')
    <input type="text" name="title" placeholder="Titre du livre" value="{{ old('title', $book->title) }}">
    <input type="text" name="summary" placeholder="Résumé" value="{{ old('summary', $book->summary) }}">

    <label for="">Séléctionner un éditeur :</label>
    <select name="editor_id">
        <option value="">Éditeurs</option>
        @foreach ($editors as $editor)
        <option value="{{ $editor->id }}"
            {{ $editor->id == old('editor_id', $book->editor_id) ? ' selected="selected"' : '' }}>{{ $editor->nom }}
        </option>
        @endforeach
    </select>


    <label for="">Séléctionner un auteur :</label>
    <select name="auteur_id">
        <option value="">Auteurs</option>
        @foreach ($auteurs as $auteur)
        <option value="{{ $auteur->id }}" {{ $auteur->id == old('auteur_id') ? ' selected="selected"' : '' }}>
            {{ $auteur->prenom }} {{ $auteur->nom }}</option>
        @endforeach
    </select>

    @php
    $categories_selected = $book->categories()->get()->pluck('id')->all();
    @endphp

    @foreach ($categories as $categorie)
    <input type="checkbox" name="categorie[]" value="{{ $categorie->id }}"
        {{ array_search($categorie->id, old('categorie', $categories_selected)) !== false ? ' checked="checked"' : '' }}">
    <label>{{ $categorie->nom }}</label>
    @endforeach

    <button type="submit">Valider</button>
</form>

@endsection