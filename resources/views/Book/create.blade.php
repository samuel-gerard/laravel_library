@extends('layouts/app')

@section('title')
    Ajout d'un livre 
@endsection

@section('content')

{{ Breadcrumbs::render('book.create') }}

<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <h2 class="mb-4">Ajout d'un livre :</h1>
                <div class="row">

    

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="{{ route('book.store') }}" method="POST">
        @csrf
        <input class="form-control" type="text" name="title" placeholder="Titre du livre" value="{{ old('title') }}">
        <input class="form-control mt-2" type="text" name="summary" placeholder="Résumé" value="{{ old('summary') }}">

        <label class="mt-4" for="">Séléctionner un éditeur :</label>
        <select class="form-control" name="editor_id">
            <option value="">Éditeurs</option>
            @foreach ($editors as $editor)
                <option value="{{ $editor->id }}"{{ $editor->id == old('editor_id') ? ' selected="selected"' : '' }}>{{ $editor->nom }}</option>
            @endforeach
        </select>
        
        <label for="">Séléctionner un auteur :</label>
        <select class="form-control" name="auteur_id">
            <option value="">Auteurs</option>
            @foreach ($auteurs as $auteur)
                <option value="{{ $auteur->id }}"{{ $auteur->id == old('auteur_id') ? ' selected="selected"' : '' }}>{{ $auteur->prenom }} {{ $auteur->nom }}</option>
            @endforeach
        </select>

        @foreach ($categories as $categorie)
            <input type="checkbox" name="categorie[]" value="{{ $categorie->id }} {{ array_search($categorie->id, old('categorie', [])) !== false ? ' checked="checked"' : '' }}">
            <label>{{ $categorie->nom }}</label>
        @endforeach

        <button class="btn btn-primary mt-2" type="submit">Valider</button>
    </form>

    </div>
</div>
</div>
</div>

@endsection