@extends('layouts/app')

@section('title')
Auteur
@endsection

@section('content')

{{ Breadcrumbs::render('book', $book) }}

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class=".col-sm">
                    @can('update', $book)
                    <a class="btn btn-primary mr-2" href="{{ route('book.edit', ['book'=>$book]) }}">Modifier le
                        livre</a>
                    @endcan
                </div>

                <div class=".col-sm">
                    @can('delete', $book)
                    <form action="{{ route('book.destroy', ['book'=>$book]) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Supprimer le livre</button>
                    </form>
                    @endcan
                </div>
            </div>
            @if ($book->auteur)
            <h2 class="mt-4">Auteur : <a
                    href="{{ route('auteur.show', ['auteur'=>$book->auteur]) }}">{{ $book->auteur->prenom }}
                    {{ $book->auteur->nom }}</a></h2>
            @endif
            @if ($book->editor)
            <h2 class="mt-4">Éditeur : <a
                    href="{{ route('editor.show', ['editor'=>$book->editor]) }}">{{ $book->editor->nom }} </a></h2>
            @endif

            <div class="card mt-4">

                <div class="card-header">
                    <h1>{{ $book->title }}</h1>
                </div>
                <div class="card-body">
                    <h3>Résumé :</h3>
                    <p>{{ $book->summary }}</p>

                    @if ($book->categories()->get()->isNotEmpty())
                    <h3>Catégorie(s) : </h2>
                        @foreach ($book->categories as $categorie)
                        <li class="">
                            <a href="{{ route('categorie.show', ['categorie'=>$categorie]) }}">{{$categorie->nom}}</a>
                        </li>
                        @endforeach
                        @endif
                </div>
            </div>



            @can('create', \App\Comment::class)

            <h2 class="mt-4">Ajouter un commentaire : </h2>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('comment.store') }}" method="POST">
                @csrf
                <input type="hidden" name="book_id" value="{{ $book->id }}">
                <textarea class="form-control" name="content" rows="5" cols="60"
                    placeholder="Nouveau commentaire">{{ old('content') }}</textarea>
                <button class="btn btn-primary mt-2" type="submit">Ajouter un commentaire</button>
            </form>
            @endcan

            @if ($book->comments()->get()->isNotEmpty())
            <h2 class="mt-3">Tous les commentaires :</h2>
            @foreach ($book->comments()->orderBy('created_at','desc')->get() as $comment)
            <div class="card container mb-2 mt-4">
                <p class="mt-3">{!! nl2br(e($comment->content), false) !!}</p>
                <p>{{ $comment->user->name }} - {{ $comment->created_at->format('m/d/Y H:i') }}</p>
            </div>

            <div class="row">
                <div class=".col-sm">
                    @can('update', $comment)
                    <a class="btn btn-sm btn-primary mr-2"
                        href="{{ route('comment.edit', ['comment'=>$comment]) }}">Modifier</a>
                    @endcan
                </div>

                <div class=".col-sm">
                    @can('delete', $comment)
                    <form action="{{ route('comment.destroy', ['comment'=>$comment]) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-sm btn-danger" type="submit">Supprimer</button>
                    </form>
                    @endcan
                </div>
            </div>





            @endforeach
            @endif

        </div>
    </div>



</div>
</div>



@endsection