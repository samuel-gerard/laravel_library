@extends('layouts/app')

@section('title')
    Catégories
@endsection

@section('content')

{{ Breadcrumbs::render('categories') }}

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @can('create', 'App\Categorie')
                    <a class="btn btn-primary mb-4" id="new_categ" href="{{ route('categorie.create') }}">Ajouter une catégorie</a>
                @endcan
                <div class="card">
                    <div class="card-header">Liste des catégories :</div>
                        <div class="card-body">
                                <ul class="list-group-item">
                                    @foreach ($categories as $categorie) 
                                        @if ($categorie)
                                            <li class="list-group-item">  
                                                <a href="{{ route('categorie.show', ['categorie'=>$categorie]) }}">{{$categorie->nom}}</a>
                                            </li>   
                                        @endif
                                    @endforeach
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    
@endsection