@extends('layouts/app')

@section('title')
Catégorie
@endsection

@section('content')

<div class="container">

    <div class="row">
        <div class=".col-sm mr-2 mb-4">
            @can('update', $categorie)
            <a class="btn btn-primary" href="{{ route('categorie.edit', ['categorie'=>$categorie]) }}">Modifier la
                catégorie</a>
            @endcan
        </div>

        <div class=".col-sm mr-2 mb-4">
            @can('delete', $categorie)
            <form action="{{ route('categorie.destroy', ['categorie'=>$categorie]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Supprimer la catégorie</button>
            </form>
            @endcan
        </div>
    </div>



    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@if ($categorie)
                    <h1>{{ $categorie->nom }}</h1>
                    @endif </div>
                <div class="card-body">
                    <h2>Livres : </h2>

                    @if ($categorie->books()->get()->isNotEmpty())
                    <ul>
                        @foreach ($categorie->books as $book)
                        <li><a href="{{ route('book.show', ['book'=>$book]) }}">{{ $book->title }}</a></li>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection