@extends('layouts/app')

@section('title')
Modification d'une catégorie
@endsection

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h1>Modification d'une catégorie :</h1>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{ route('categorie.update', ['categorie'=>$categorie]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input class="form-control" type="text" name="nom" placeholder="Nom de la catégorie"
                        value="{{ old('categorie', $categorie->nom) }}">
                    <button class="btn btn-primary mt-2" type="submit">Valider</button>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection