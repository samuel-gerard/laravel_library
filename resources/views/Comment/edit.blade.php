@extends('layouts/app')

@section('title')
Edit comment #{{$comment->id}}
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <h2>Modification du {{$comment->id}}ème commentaire :</h2>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{ route('comment.update', ['comment'=>$comment]) }}" method="post">
                    @csrf
                    @method('PUT')
                    <textarea class="form-control" name="content" placeholder="Content" rows="5"
                        cols="60">{{ old('content', $comment->content) }}</textarea>
                    <button class="btn btn-primary mt-2" type="submit">Modifier</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection