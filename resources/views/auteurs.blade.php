@extends('layouts/app')

@section('title')
    Books
@endsection

@section('content')

{{ Breadcrumbs::render('auteurs') }}

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @can('create', 'App\Auteur')
                    <a class="btn btn-primary mb-4" href="{{ route('auteur.create') }}">Ajouter un éditeur</a>
                @endcan
                <div class="card">
                    <div class="card-header">Liste des auteurs : </div>
                        <div class="card-body">
                            <ul class="list-group-item">
                                @foreach ($auteurs as $auteur)
                                    @if ($auteur)
                                    <li class="list-group-item">
                                        <a href="{{ route('auteur.show', ['auteur'=>$auteur]) }}">{{ $auteur->prenom }} {{ $auteur->nom }}</a>
                                    </li>
                                     @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection