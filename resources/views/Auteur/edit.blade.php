@extends('layouts/app')

@section('title')
{{ $auteur->prenom }} {{ $auteur->nom }}
@endsection

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h1>Modification de l'auteur {{ $auteur->prenom }} {{ $auteur->nom }}</h1>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{ route('auteur.update', ['auteur'=>$auteur]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input class="form-control" type="text" name="nom" placeholder="lastname" value="{{ old('nom', $auteur->nom) }}">
                    <input class="form-control mt-2" type="text" name="prenom" placeholder="firstname"
                        value="{{ old('prenom', $auteur->prenom) }}">
                    <button class="btn btn-primary mt-2" type="submit">Valider</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection