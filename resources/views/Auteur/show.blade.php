@extends('layouts/app')

@section('title')
Auteur
@endsection

@section('content')

{{ Breadcrumbs::render('auteur', $auteur) }}

<div class="container">

    <div class="row">
        <div class=".col-sm mr-2 mb-4">
            @can('update', $auteur)
            <a href="{{ route('auteur.edit', ['auteur'=>$auteur]) }}" class="btn btn-primary">Modifier l'auteur</a>
            @endcan
        </div>

        <div class=".col-sm mr-2 mb-4">
            @can('delete', $auteur)
            <form action="{{ route('auteur.destroy', ['auteur'=>$auteur]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Supprimer l'auteur</button>
            </form>
            @endcan
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h1>Auteur : {{ $auteur->prenom }} {{ $auteur->nom }}</h1>
                </div>
                <div class="card-body">
                    <ul>
                        @foreach ($auteur->books as $book)
                        <li><a href="{{ route('book.show', ['book'=>$book]) }}">{{ $book->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection