@extends('layouts/app')

@section('title')
    Ajout d'un auteur 
@endsection

@section('content')

<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                        <h1>Ajout d'un auteur :</h1>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        
                        <form action="{{ route('auteur.store') }}" method="POST">
                            @csrf
                            <input class="form-control" type="text" name="nom" placeholder="Nom de l'auteur" value="{{ old('nom') }}">
                            <input class="form-control mt-2" type="text" name="prenom" placeholder="Prenom de l'auteur" value="{{ old('prenom') }}">
                            <button class="btn btn-primary mt-2" type="submit">Valider</button>
                        </form>
                </div>
            </div>
        </div>
    </div>

    

@endsection