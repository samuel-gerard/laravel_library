<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Support\Facades\App;

class Book extends Model
{
    protected $fillable = ['title', 'summary', 'auteur_id', 'editor_id'];

    function auteur(){
        return $this->belongsTo('App\Auteur');
    }

    function editor(){
        return $this->belongsTo('App\Editor');
    }

    function categories(){
        return $this->belongsToMany('App\Categorie');
    }

    function comments(){
        return $this->hasMany('App\Comment');
    }
}
