<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auteur extends Model
{
    protected $fillable = ['nom', 'prenom'];

    function books(){
        return $this->hasMany('App\Book');
    }
}
