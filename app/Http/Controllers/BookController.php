<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;

class BookController extends Controller
{

    function __construct()
    {
        $this->authorizeResource('App\Book');
    }

    function index(){
        $this->authorize('viewAny', \App\Book::class);
        $books = \App\Book::all();
        return view('books', ['books'=>$books]);
    }

    function show(\App\Book $book){
        return view('Book.show', ['book'=>$book]);
    }

    function create(){
        $auteurs = \App\Auteur::all();
        $editors = \App\Editor::all();
        $categories = \App\Categorie::all();
        return view('Book.create', ['auteurs'=>$auteurs, 'editors'=>$editors, 'categories'=>$categories]);
    }

    function store(\App\Http\Requests\BookRequest $request){
        $data = $request->validated();
        $book = new \App\Book();
        $book->fill($data);
        $book->save();
        if(isset($data['categorie'])){
            $book->categories()->sync($data['categorie']);
        }        
        return redirect()->route('book.show', ['book'=>$book]);
    }

    function edit(\App\Book $book){
        $auteurs = \App\Auteur::all();
        $editors = \App\Editor::all();
        $categories = \App\Categorie::all();
        return view('Book.edit', ['book'=>$book, 'auteurs'=>$auteurs, 'editors'=>$editors, 'categories'=>$categories]);
    }

    function update(\App\Book $book, \App\Http\Requests\BookRequest $request){
        $data = $request->validated();
        $book->fill($data);
        $book->save();
        $book->categories()->sync($data['categorie']);
        return redirect()->route('book.show', ['book'=>$book]);
    }

    function destroy(\App\Book $book){
        $book->delete();
        return redirect()->route('books');
    }
}
