<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuteurController extends Controller
{

    function __construct()
    {
        $this->authorizeResource('App\Auteur');
    }

    function index(){
        $auteur = \App\Auteur::all();
        return view('auteurs', ['auteurs'=>$auteur]);
    }

    function show(\App\Auteur $auteur){
        return view('Auteur.show', ['auteur'=>$auteur]);
    }

    function create(){
        return view('Auteur.create');
    }

    function store(\App\Http\Requests\AuteurRequest $request){
        $data = $request->validated();
        $auteur = new \App\Auteur();
        $auteur->fill($data);
        $auteur->save();
        return redirect()->route('auteur.show', ['auteur'=>$auteur]);
    }

    function edit(\App\Auteur $auteur){
        return view('Auteur.edit', ['auteur'=>$auteur]);
    }

    function update(\App\Http\Requests\AuteurRequest $request, \App\Auteur $auteur){
        $data = $request->validated();
        $auteur->fill($data);
        $auteur->save();
        return redirect()->route('auteur.show', ['auteur'=>$auteur]);
    }

    function destroy(\App\Auteur $auteur){
        $auteur->delete();
        return redirect()->route('auteurs');
    }
}
