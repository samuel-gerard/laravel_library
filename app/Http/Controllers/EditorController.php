<?php

namespace App\Http\Controllers;

use App\Editor;
use Illuminate\Http\Request;
use App\Http\Requests\EditorRequest;


class EditorController extends Controller
{

    function __construct()
    {
        $this->authorizeResource('App\Editor');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Editor.index', ['editors'=>Editor::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Editor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EditorRequest $request)
    {
        $data = $request->validated();
        $editor = new \App\Editor();
        $editor->fill($data);
        $editor->save();
        return redirect()->route('editor.show', ['editor'=>$editor]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Editor  $editor
     * @return \Illuminate\Http\Response
     */
    public function show(Editor $editor)
    {
        return view('Editor.show', ['editor'=>$editor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Editor  $editor
     * @return \Illuminate\Http\Response
     */
    public function edit(Editor $editor)
    {
        return view('Editor.edit', ['editor'=>$editor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Editor  $editor
     * @return \Illuminate\Http\Response
     */
    public function update(EditorRequest $request, Editor $editor)
    {
        $data = $request->validated();
        $editor->fill($data);
        $editor->save();
        return redirect()->route('editor.show', ['editor'=>$editor]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Editor  $editor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Editor $editor)
    {
        $editor->delete();
        return redirect()->route('editor.index');
    }
}
