<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $fillable = ['nom'];

    function books(){
        return $this->belongsToMany('App\Book');
    }
}
