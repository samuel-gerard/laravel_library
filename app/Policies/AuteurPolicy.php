<?php

namespace App\Policies;

use App\User;
use App\Auteur;
use Illuminate\Auth\Access\HandlesAuthorization;

class AuteurPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any auteurs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the auteur.
     *
     * @param  \App\User  $user
     * @param  \App\Auteur  $auteur
     * @return mixed
     */
    public function view(User $user, Auteur $auteur)
    {
        return true;
    }

    /**
     * Determine whether the user can create auteurs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the auteur.
     *
     * @param  \App\User  $user
     * @param  \App\Auteur  $auteur
     * @return mixed
     */
    public function update(User $user, Auteur $auteur)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can delete the auteur.
     *
     * @param  \App\User  $user
     * @param  \App\Auteur  $auteur
     * @return mixed
     */
    public function delete(User $user, Auteur $auteur)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can restore the auteur.
     *
     * @param  \App\User  $user
     * @param  \App\Auteur  $auteur
     * @return mixed
     */
    public function restore(User $user, Auteur $auteur)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can permanently delete the auteur.
     *
     * @param  \App\User  $user
     * @param  \App\Auteur  $auteur
     * @return mixed
     */
    public function forceDelete(User $user, Auteur $auteur)
    {
        return $user->is_admin;
    }
}
