<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Accueil', route('home'));
});

// Home > About
Breadcrumbs::for('about', function ($trail) {
    $trail->parent('home');
    $trail->push('Accueil', route('about'));
});

// Home > Books
Breadcrumbs::for('books', function ($trail) {
    $trail->parent('home');
    $trail->push('Livres', route('books'));
});

// Home > Authors
Breadcrumbs::for('auteurs', function ($trail) {
    $trail->parent('home');
    $trail->push('Auteurs', route('auteurs'));
});

// Home > Editors
Breadcrumbs::for('editors', function ($trail) {
    $trail->parent('home');
    $trail->push('Editeurs', route('editor.index'));
});

// Home > Categories
Breadcrumbs::for('categories', function ($trail) {
    $trail->parent('home');
    $trail->push('Catégories', route('categorie.index'));
});

// Home > Authors -> Author
Breadcrumbs::for('auteur', function ($trail, $auteur) {
    $trail->parent('auteurs');
    $trail->push($auteur->nom, route('auteur.show', $auteur->id));
});

// Home > Editors -> Editor
Breadcrumbs::for('editor', function ($trail, $editor) {
    $trail->parent('editors');
    $trail->push($editor->nom, route('editor.show', $editor->id));
});

// Home > Books -> Book
Breadcrumbs::for('book', function ($trail, $book) {
    $trail->parent('books');
    $trail->push($book->title, route('book.show', $book->id));
});

// Home > Books -> Book -> Edit
Breadcrumbs::for('book.edit', function ($trail, $book) {
    $trail->parent('book', $book);
    $trail->push('Modifier', route('book.edit', ['book'=>$book]));
});

// Home > Books -> Create
Breadcrumbs::for('book.create', function ($trail) {
    $trail->parent('books');
    $trail->push('Ajouter un livre', route('books'));
});
