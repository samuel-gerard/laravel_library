<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function(){

    // ---------- Gestion des pages de bases ---------- //

    Route::get('/books', 'BookController@index')
    ->name('books');

    Route::get('/auteurs', 'AuteurController@index')
    ->name('auteurs');

    Route::get('/auteur/{auteur}', 'AuteurController@show')
    ->name('auteur.show')->where('auteur', '[0-9]+');

    Route::get('/book/{book}', 'BookController@show')
    ->name('book.show')->where('book', '[0-9]+');


    // ---------- Gestion des auteurs ---------- //

    Route::get('/auteur/create', 'AuteurController@create')
    ->name('auteur.create');

    Route::post('/auteur/create', 'AuteurController@store')
    ->name('auteur.store');

    Route::delete('/auteur/{auteur}', 'AuteurController@destroy')
    ->name('auteur.destroy')->where('auteur', '[0-9]+');

    Route::get('/auteur/{auteur}/edit','AuteurController@edit')
    ->name('auteur.edit')->where('auteur', '[0-9]+');

    Route::put('/auteur/{auteur}/edit','AuteurController@update')
    ->name('auteur.update')->where('auteur', '[0-9]+');


    // ---------- Gestion des books ---------- //

    Route::get('/book/create', 'BookController@create')
    ->name('book.create');

    Route::post('/book/create', 'BookController@store')
    ->name('book.store');

    Route::delete('/book/{book}', 'BookController@destroy')
    ->name('book.destroy')->where('book', '[0-9]+');

    Route::get('/book/{book}/edit', 'BookController@edit')
    ->name('book.edit')->where('book', '[0-9]+');

    Route::put('/book/{book}/edit', 'BookController@update')
    ->name('book.update')->where('book', '[0-9]+');


    // ---------- Gestion des éditeurs ---------- //
    Route::resource('/editor', 'EditorController');


    // ---------- Gestion des catégories ---------- //
    Route::resource('/categorie', 'CategorieController');


    // ---------- Gestion des commentaires ---------- //
    Route::resource('/comment', 'CommentController')->except(['index', 'show', 'create']);


});


// ---------- Gestion des pages de bases ---------- //

Route::get('/', 'HomeController@index')
->name('home');

Route::get('/about', 'HomeController@about')
->name('about');

// ---------- Gestion des authentifications ---------- //
Auth::routes(['register' => true]);